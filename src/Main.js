import React from 'react';
import Logo from './Components/AnimatedLogo';
import ContactForm from './Components/ContactForm';
import Particles from 'react-particles-js';
import './index.scss';
import Modal from 'react-awesome-modal';

const particleOpt = {
  particles: {
    number: {
      value: 35
    },
    size: {
      value: 2.5
    },
    color: {
      value: '#D4AF37'
    },
    move: {
      direction: 'top',
      out_mode: 'out',
      speed: 2
    },
    line_linked: {
      // color: '#3f51b5'
    }
  },
  interactivity: {
    events: {
      onhover: {
        enable: true,
        mode: 'repulse'
      },
      onclick: {
        enable: true,
        mode: 'push'
      }
    }
  }
};

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.roles = ['plan', 'design', 'develop', 'test', 'deploy'];
    this.state = {
      visible: false
    };
  }

  openModal() {
    this.setState({
      visible: true
    });
  }

  closeModal() {
    this.setState({
      visible: false
    });
  }
  render() {
    return (
      <div className="main-div">
        <Particles params={particleOpt} className="particles" />
        <div className="main-components">
          <Logo />
          {/* <button onClick={() => this.openModal()}>hello</button> */}
          <input
            className="main-contact--button"
            type="button"
            value="Contact Us"
            onClick={() => this.openModal()}
          />
          <Modal
            visible={this.state.visible}
            className="modal"
            effect="fadeInUp"
            onClickAway={() => this.closeModal()}
          >
            <div>
              <p
                style={{
                  fontSize: '20px',
                  padding: '0 20px 0 20px',
                  fontWeight: 'bold',
                  textAlign: 'center',
                  marginBottom: '0'
                }}
              >
                GET IN TOUCH
              </p>
              <p
                style={{
                  fontSize: '15px',
                  padding: '0 20px 0 20px',
                  textAlign: 'center',
                  marginTop: '0'
                }}
              >
                WE'LL GET BACK TO YOU AS SOON AS POSSIBLE
              </p>
              <ContactForm />
            </div>
          </Modal>
        </div>
      </div>

      // <div>
      //   <Particles params={particleOpt} />
      // </div>
    );
  }
}

export default Main;
