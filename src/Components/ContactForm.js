import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
// import MenuItem from '@material-ui/core/MenuItem';
import axios from 'axios';

// const subjectOpt = [
//   {
//     value: 'E-Commerce Platform'
//   },
//   {
//     value: 'Custom Website'
//   },
//   {
//     value: 'Marketing Website'
//   },
//   {
//     value: 'iOS App'
//   },
//   {
//     value: 'Graphic Design'
//   },
//   {
//     value: 'Social Media'
//   },
//   {
//     value: 'Other'
//   }
// ];

class ContactForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      subject: '',
      message: '',
      sent: false,
      buttonText: 'SUBMIT',
      formErrors: {
        name: '',
        email: '',
        subject: '',
        message: ''
      },
      nameValid: false,
      emailValid: false,
      subjectValid: false,
      messageValid: false,
      isValid: false
    };
  }

  handleChange = e => {
    e.preventDefault();

    const { name, value } = e.target;

    let formErrors = { ...this.state.formErrors };
    let nameValid = this.state.nameValid;
    let emailValid = this.state.emailValid;
    let subjectValid = this.state.subjectValid;
    let messageValid = this.state.messageValid;

    switch (name) {
      case 'name':
        nameValid = value.length >= 1;
        formErrors.name = nameValid ? '' : 'Please provide your name.';
        break;
      case 'email':
        emailValid = value.match(
          /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
        );
        formErrors.email = emailValid ? '' : 'Invalid email address.';
        break;
      case 'subject':
        subjectValid = value.length >= 1;
        formErrors.subject = subjectValid ? '' : 'Please select an subject';
        break;
      case 'message':
        messageValid = value.length >= 1;
        formErrors.message = messageValid
          ? ''
          : 'Please provide a short description.';
        break;
      default:
        break;
    }

    this.setState(
      {
        formErrors,
        [name]: value,
        nameValid: nameValid,
        emailValid: emailValid,
        subjectValid: subjectValid,
        messageValid: messageValid
      },
      this.validation
    );
  };

  validation() {
    this.setState({
      isValid:
        this.state.nameValid &&
        this.state.emailValid &&
        this.state.subjectValid &&
        this.state.messageValid
    });
  }

  handleFormSubmit = e => {
    e.preventDefault();

    this.setState({
      buttonText: 'SENDING'
    });

    let data = {
      name: this.state.name,
      email: this.state.email,
      subject: this.state.subject,
      message: this.state.message
    };

    axios
      .post('https://formcarry.com/s/XXTNP7051eH', data, {
        headers: { Accept: 'application/json' }
      })
      .then(res => {
        this.setState({ sent: true }, this.resetForm());
      })
      .catch(() => {
        this.setState({ buttonText: 'Sending Failed :(' });
      });
  };

  resetForm = () => {
    this.setState({
      name: '',
      message: '',
      email: '',
      subject: '',
      buttonText: 'SENT'
    });
  };

  render() {
    return (
      <div className="test">
        <TextField
          id="name"
          label="Name *"
          className="input"
          type="text"
          name="name"
          autoComplete="name"
          margin="normal"
          variant="outlined"
          value={this.state.name}
          onChange={this.handleChange}
        />

        <TextField
          id="email"
          label="Email *"
          className="input"
          type="email"
          name="email"
          autoComplete="email"
          margin="normal"
          variant="outlined"
          value={this.state.email}
          onChange={this.handleChange}
        />

        <TextField
          id="subject"
          label="Subject *"
          className="input"
          type="text"
          name="subject"
          autoComplete="subject"
          margin="normal"
          variant="outlined"
          value={this.state.subject}
          onChange={this.handleChange}
        />

        {/* Dropdown - having issues with Modal */}
        {/* <TextField
          select
          id="subject"
          label="Subject *"
          className="input"
          type="text"
          name="subject"
          autoComplete="subject"
          margin="normal"
          variant="outlined"
          value={this.state.subject}
          onChange={this.handleChange}
        >
          {subjectOpt.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.value}
            </MenuItem>
          ))}
        </TextField> */}

        <TextField
          id="message"
          label="Message *"
          className="input"
          type="text"
          name="message"
          autoComplete="message"
          margin="normal"
          variant="outlined"
          value={this.state.message}
          onChange={this.handleChange}
        />
        <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
          <button
            className="contact-button"
            onClick={e => this.handleFormSubmit(e)}
            disabled={!this.state.isValid}
          >
            {this.state.buttonText}
          </button>
        </div>
      </div>
    );
  }
}

export default ContactForm;
